from flask import Flask, request
from flask.views import MethodView

app = Flask(__name__)

@app.errorhandler(500)
def internal_error(error):
    return "Cannot retrieve what you are looking for right now"

class Request(MethodView):
    def post(self):
        outcome = request.json['outcome']
        body = outcome['_text']
        if request.json['lang'] == 'fr':
            answer = "Je ne sais pas quoi dire"
            if "c'est la faute a qui" in body.lower() or "c'est la faute de qui" in body.lower() or "c'est la faute" in body.lower():
                answer = "C'est la faute à Miramo !"
            if "qui est ton createur" in body.lower() or "qui t'a" in body.lower():
                answer = "J'ai été crée par Martin Lequeux--Gruninger et son équipe"
            if "est-ce que tu m'aime" in body.lower():
                answer = "Biensur que oui !"
            if "merci" in body.lower() or "merci beaucoup" in body.lower():
                answer = "De rien !"
            return answer
        elif request.json['lang'] == 'en':
            answer = "I don't know what to say"
            if "who's to blame" in body.lower() or "who is to blame" in body.lower() or "whos to blame" in body.lower():
                answer = "It is Miramo's fault !"
            if "who's your creator" in body.lower() or "who is your creator" in body.lower() or "whos your creator" in body.lower():
                answer = "I was created by Martin Lequeux--Gruninger and his team"
            if "do you love me" in body.lower():
                answer = "Off course I do !"
            if "thank you" in body.lower() or "thanks" in body.lower() or "thank" in body.lower() or "thank's" in body.lower():
                answer = "You're welcome"
            return answer


if __name__ == '__main__':
    app.add_url_rule('/', view_func=Request.as_view('request'))
    app.run(host='0.0.0.0', port=80)
